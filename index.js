const restify = require('restify');

const SERVER_PORT = 3000;

const server = restify.createServer({ name: 'App Service' });

server.listen(SERVER_PORT, () => {
  console.log(`Server running on: ${JSON.stringify(server.address(), undefined, 2)}`);
});

module.exports = server;
