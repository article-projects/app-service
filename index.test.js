/* eslint-disable global-require */

describe('Unit test example', () => {
  const mockCreate = jest.fn();
  const mockListen = jest.fn();

  beforeEach(() => {
    jest.mock('restify', () => {
      const restify = jest.genMockFromModule('restify');
      const dummyServer = { listen: mockListen };
      mockCreate.mockReturnValueOnce(dummyServer);
      restify.createServer = mockCreate;
      return restify;
    });
  });

  test('Should start Restify with right function calls', () => {
    require('./index.js');
    expect(mockCreate).toHaveBeenCalledTimes(1);
    expect(mockListen).toHaveBeenCalledTimes(1);
  });
});
