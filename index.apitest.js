/* eslint-disable global-require */
global.console = { log: jest.fn() };
const request = require('supertest');

describe('API test example', () => {
  let server;

  beforeAll(() => {
    server = require('./index');
  });

  afterAll(() => {
    server.close();
  });

  test('Should return a client error for #get /', done => (
    request(server)
    .get('/')
    .expect(404, {
      code: 'ResourceNotFound',
      message: '/ does not exist',
    })
    .end((err) => {
      if (err) return done.fail(err);
      return done();
    })
  ));
});
